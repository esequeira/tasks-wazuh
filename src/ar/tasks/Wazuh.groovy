#!/usr/bin/env groovy

package ar.tasks



class Wazuh {
  String type
  String branchTag
  String operatingSystem
  String architecture

	//constructor
  private Wazuh(String type, String branchTag, String operatingSystem, String architecture) {
    this.type = type
    this.branchTag = branchTag
    this.operatingSystem = operatingSystem
    this.architecture = architecture
  }

  public def print() {
    println "Type: ${this.type}"
    println "Branch/Tag: ${this.branchTag}"
    println "Operating System: ${this.operatingSystem}"
    println "Architecture: ${this.architecture}"
  }

  @NonCPS
  public def build() {
    def downloadUrl = "https://github.com/wazuh/wazuh/archive/${this.branchTag}.tar.gz"
    def downloadDest = "/tmp/wazuh-${this.branchTag}.tar.gz"
    new URL(downloadUrl).withInputStream { input ->
      new File(downloadDest).withOutputStream { output ->
        output << input
      }
    }
  }

  @NonCPS
  def packages() {
      def command = './tmp/gitClone.sh'
      def runtime = Runtime.getRuntime()
      def process = runtime.exec(command)
      process.waitFor()
  }
}